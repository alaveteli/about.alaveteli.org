---
title : "Hur fungerar Handlingar.se?"
description: "Handlingar.se är plattformen för dig som behöver information från offentlig sektor."
draft: false
weight: 2
---

Plattformen Handlingar.se hjälper dig inom näringsliv, myndighet eller förening att få information du behöver från offentlig sektor. Plattformen hjälper även dig som jobbar inom myndighet att ligga ett steg före dina användare. Med handlingar.se kan du automatisera tillgängliggörande av handlingar innan du får frågan. Du som använder handlingar.se kan begära ut eller hitta information som kan skapa värde och utveckling för dig, din verksamhet och samhället.
