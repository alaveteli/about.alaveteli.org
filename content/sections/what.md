---
title : "Vad är Handlingar.se?"
description: ""
draft: false
weight: 3
---

Handlingar.se är en plattform som med tjänster som ger en inblick in i framtidens offentliga förvaltning. Vi erbjuder tjänster för informationsinhämtning och analys för offentlig sektor, näringslivet, civilsamhället och privatpersoner.
