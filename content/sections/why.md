---
title : "Digitala handlingar från myndigheter? Varför då?"
description: ""
draft: false
weight: 1
---

Det är våra handlingar som definierar oss som människor, organisationer och samhälle. Handlingar.se hjälper dig att få information från alla organisationer i offentlig sektor - på ett och samma ställe. Handlingar.se är plattformen för dig som behöver information från offentlig sektor. Coronakrisen i Sverige tydliggör behovet av tillgång till allmänna handlingar - digitalt och kostnadsfritt. Utan information kan du och dina handlingar inte leva upp till sin potential. När fler har viktig information kan fler rädda liv, gemenskaper och organisationer. Fler kan utvecklas i en hållbar riktning.
