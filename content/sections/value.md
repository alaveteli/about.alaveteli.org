---
title : "Vad kan Handlingar.se göra för dig?"
description: ""
draft: false
weight: 3
---

Sverige har idag tusentals myndigheter i offentlig sektor. Myndigheter har information som kan skapa värde för dig. Denna information är skattefinansierad information som du är delägare av. Det betyder att du har rätt att ta del av informationen och skapa värde med den.
