---
title: "Alpha-test av Handlingar.se"
description: "Nu lanserar vi första testperioden!"
date: 2019-02-01T21:00:00+01:00
publishDate: 2019-02-01T21:00:00+01:00
author: "Handlingar-gruppen"
images: []
draft: false
tags: ["alpha", "prototyp"]
---


Hej världen! Välkommen till bloggen för Handlingar.se!

Här kommer vi som driver plattformen att skriva sporadiska uppdateringar för att dela med oss när vi har något nytt att berätta om utvecklingen av projektet Handlingar.se.

Vi som driver plattformen tycker att offentlighetsprincipen är en otroligt bra tillgång för värdeskapande på flera sätt. Genom att offentlighetsprincipen kan bli
mer digital kan också samhället dra mer nytta av vår grundlag. Det här kommer vi att återkomma till i kommande blogginlägg framöver.

Vi hörs snart igen!

Bästa hälsningar,
Handlingar-gruppen
