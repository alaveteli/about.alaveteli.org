---
title: "Handlingar Report from AlaveteliCon in Oslo"
description: "Sharing insights from the Freedom of Information conference by MySociety"
date: 2019-10-01T21:00:00+01:00
publishDate: 2019-10-01T21:00:00+01:00
author: "Sushma Giri"
images: []
draft: false
tags: ["code", "community", "Alaveteli",]
---


Alaveteli Conference 2019, Freedom of Information Technologies: Collaboration, Funding and Knowledge Sharing

Team members from Handlingar.se the Swedish version of Alaveteli: Freedom of Information (FOI) Technologies participated in Alaveteli Con 2019 from September 23-24 held in Oslo, Norway. Handlingar was started early this year and participating in the conference at this point was an excellent opportunity for us. The conference was held five years after the one in Madrid in 2015. It looked like the conference was waiting for us to start our platform and join them.

![AlaveteliCon-2019](https://gitlab.com/handlingar/om.handlingar.se/-/blob/master/themes/terrassa/static/images/AlaveteliCon-2019.jpg)

            Photo: AlaveteliCon 2019 Participants


The two-day conference was successful to bring around 50 journalists, researchers, technologists, and activists from 18 different countries. A very big thank you and congratulations to the whole of mySociety team for making this conference possible. The unconference format of presentation, activities fostered communication and knowledge sharing between participants. The conference was well managed with ample of interactive activities and presentations. My personal favorite activity was the Cards Against Humanity game. Through the game, we came to know about the struggles of FOI activists and journalists while working with authorities in their countries.

The level of success WhatDoTheyKnow and MuckRock have achieved over these years are exemplary for all of us working with common vision. Also, a very unique, narrow-focused use of the Alaveteli platform Frag Den Staat in Germany for food safety campaign (Topf Secret) drew my attention. Topf Secret does crowdsourced food safety inspections along with the pre-filled request form to the right food authority.

One of the observations that overwhelmed us was the fact that a lot of participants were working voluntarily, some were even working on their platform as a side project besides their job. I could feel the energy and the spirit of a true FOI activist during a personal conversation with all of them. There was a much needed session on funding where Frag Den Staat shared about online art selling, MuckRock and WhatDoTheyKnow shared about donations from the user as fund source. Cross learning of available funding opportunities has been done among the participants.

The conference has sparked multiples ideas to take our platform forward in the coming days. FOI curriculum for kids, students, and volunteers is one of them. Also, narrowing the focusing or creating a campaign for a particular issue like that of Topf Secret in Germany can be a possible work. We also plan to upgrade and use Alavateli Pro on our platform. Huge thanks to the developers of Alaveteli from MySociety for technical support during the conference for our platform.

We hope we will be able to make significant progress with Handlingar.se and take it to the next level before the next Alaveteli Conference.
