---
title: "Vi importerar alla Sveriges myndigheter på Handlingar.se"
description: "Som myndigheterna själva offentliggjort"
date: 2019-03-03T21:00:00+01:00
publishDate: 2019-03-03T21:00:00+01:00
author: "Handlingar-gruppen"
images: [2016-visualization-swedish-authorities.jpeg]
draft: false
tags: ["myndigheter", "SKR", "SCB", "Myndighetsregistret",]
---

Den senaste tiden har vi arbetat med att samla ihop och organisera myndigheters kontaktuppgifter.

Nyligen lyckades vi importera alla myndigheter och deras kontaktuppgifter på Handlingar.se.

Dessa myndighter är alla de myndigheter som vi kan hitta offentligt publicerade via SCB:s (Statistiska Centralbyråns) så kallade Myndighetsregister för statliga myndigheter.

För kommuner och regioner, som fram tills nyligen kallades landsting så har vi behövt begära ut data från SKL (uppdatering 2020: SKR).

All denna data har vi satt ihop i ett och samma dataset. Det finns även planer på att importera denna data på Wikidata-projektet.

Vi hoppas så klart att svenska myndigheter ska ge ut namn och kontaktuppgifter till alla myndigheter med en registratur som ett öppna dataset.

På återhörande,
Handlingar-gruppen
