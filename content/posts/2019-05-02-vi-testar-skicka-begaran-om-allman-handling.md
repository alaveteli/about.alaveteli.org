---
title: "Vi testar skicka begäran om allmän handling"
description: "Genom att använda Handlingar.se!"
date: 2019-05-02T21:00:00+01:00
publishDate: 2019-05-02T21:00:00+01:00
author: "Handlingar-gruppen"
images: []
draft: false
tags: ["alpha", "test"]
---

Idag testade vi skicka vår första begäran om allmän handling!

Vi som driver Handlingar.se gör detta med begränsad tid och resurser. Därför tar saker längre tid än vad vi vill.

Nu har vi som sagt i alla fall testat i skarpt läge och vi ser fram emot att många fler frågor kommer kunna skickas framöver!

Vi hörs snart igen!
Handlingar-gruppen
