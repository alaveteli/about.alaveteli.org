---
title: "Vi provar att aktivera serieförfrågningar"
description: "För att lägga grund till värdeskapande i systematisk skala!"
date: 2019-08-30T21:00:00+01:00
publishDate: 2019-08-30T21:00:00+01:00
author: "Handlingar-gruppen"
images: []
draft: false
tags: ["alpha", "test", "serieförfrågningar",]
---


På begäran från en av våra tidiga användare på Handlingar.se har vi nu aktiverat serieförfrågningar.

Funktionen är väldigt kraftfull och vi vet att den kan göra stor nytta, "men med stor kraft kommer ett stort ansvar".

Därför ska den här funktionen användas medvetet och med måttlighet för att också ge bra resultat.

Det är viktigt för oss att funktionerna vi aktiverar och implementerar på Handlingar.se skapar nytta och värde för många och därför gör vi avvägningar i när och hur vi rullar ut olika funktioner.

Vi vet att serieförfrågningar kan användas för att t.ex. göra jämförelser mellan resultat. Därför kan funktionen komma till användning i många olika fallstudier. Det kan användas av bland annat akademi för forskning, entreprenörer som arbetar mot offentlig sektor och intresseorganisationer som vill driva sin intressefråga.

Förhoppningsvis kommer du som läser kunna dra nytta av denna funktion i framtiden! Den som frågar får se!

Tills dess,
Handlingar-gruppen
