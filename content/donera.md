---
title: "Donera"
description: "Handlingar drivs av ideella krafter"
images: ["undraw_private_data_orange.svg"]
draft: true
menu: main
weight: 6
---

Gillar du vad vi gör? Du kan donera till rörelsen som driver frågan informationshantering i offentlig sektor och digitalt tillgängliggörande av information från offentlig sektor.

### Swish

### OpenCollective

### PayPal

### LiberaPay

### Patreon
