---
title: "Om"
description: "Lär dig mer om bakgrunden till Handlingar.se och om verksamheten"
images: ["undraw_meet_the_team_orange.svg"]
draft: false
menu: main
weight: 2
---

# Vad vi tror på


Handlingar.se grundades 2019 utifrån tanken att information genom allmänna handlingar och öppna data i offentlig sektor endast är värdefulla om de kan användas. Lärande och utveckling kan inte ske utan möjligheten till nya insikter och förståelse. Vår idé är att hjälpa myndigheter till att digitalt tillgängliggöra information som kan hjälpa myndigheter, företag, civilsamhälle och människor att dra nytta av dem för att utveckla sig, sina verksamheter och samhället. Med dagens enorma mängder information i offentlig sektor har många människor inte ens kommit igång att nyttja dessa tillgångar som kan hjälper dem skapa och förnya sina åtaganden och verksamheter. Vi vill möjliggöra upplysta handlingar, förnyelse och utveckling hos varje människa och organisation. När fler får tillgång till information kan det göra samhället härligare att leva i och framtiden något att se fram emot.

Handlingar.se ger varje användare och organisation ett verktyg för att utvecklas. Deras idéer och nyfikenhet kan lätt delas i stor skala för att tillgängliggöra nya insikter och driva utvecklingen framåt av fler. Vi på Handlingar.se vill att kontinuerligt lärande och utveckling ska vara en naturlig effekt av användningen av information hos offentlig sektor.
