---
title: "Föreningar"
description: "Tjänster för ekonomiska och ideella föreningar i civilsamhället"
images: ["undraw_next_option_orange.svg"]
draft: false
menu: main
weight: 5
---

# Handlingar Förening

Information för din sak!

**Abonnemang och priser**

395 kr / månad (max 5 användare)

# Förmåner

## Pro-funktion med bl.a. privata förfrågningar

## Dolda serieförfrågningar - analysera och organisera er data inför nästa kampanj eller projekt

## Guider för förfrågningar om allmänna handlingar

## Möjlighet till dedicerade guider

# Framgångsberättelser

Här kommer Handlingar-teamet fylla på med framgångsberättelser för Handlingar.se för ideella föreningar.

## Projektet Öppen Upphandling

I starten av projektet använde Öppen Upphandling-gruppen Handlingar.se för att ta reda på hur det ligger till med situationen kring fakturor i Sverige. etc etc.


# Frågor och svar

## Hur skaffar jag Handlingar.se för ideella föreningar?

För att vara kvalificerad för Handlingar.se för ideella föreningar måste verksamheten vara en registrerad ideell förening.


## Finns det mer material för att lära sig om allmänna handlingar?

Genom att prenumerera på Handlingar Förening kan vi ta fram och tillgängliggöra mer material som kan hjälpa dig i ditt arbete med allmänna handlingar och öppna data. Det går även att boka Handlingar-gruppen för föreläsning, workshop eller annan skräddarsydd utbildning eller konsultation. Detta kan hjälpa er lyckas uppgradera företagets verksamhet med kraftfull information och data genom era förfrågningar om allmänna handlingar.

## Om jag är en godkänd ideell förening, kan jag ändå prenumerera på de kommersiella alternativen?

Ja. Även om ni kvalificerar som ideell förening, så går det alltid att köpa de kommersiella alternativen. Med Handlingar Ideell förening möjliggörs att betala via faktura utan moms.
